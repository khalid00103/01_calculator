package pro1;

public class NumericUtils {
    // gcd pro kladná čísla
    public static long gcd(long a, long b)
    {
        if (a<= 0 || b<=0)
        {
            ArithmeticException ae = new ArithmeticException();
            throw ae;
        }

        if(a<b)
        {
          long pomocnaPrommenna = a;
          a = b;
          b = pomocnaPrommenna;
        }

        while (b > 0)
        {
            long noveA = b;
            long noveB = a % b;

            b = noveB; // chci aby b mělo hodnotu a%b
            a = noveA; // chci aby a mělo hodnotu jako mělo předtím b
        }

        return a;
    }
}
