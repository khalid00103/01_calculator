package pro1;

import static org.junit.jupiter.api.Assertions.*;

class FractionTest {

    @org.junit.jupiter.api.Test
    void parse() {
        assertEquals(
            new Fraction(11,-1),
            new Fraction(-11,1)
        );
        Fraction f = Fraction.parse("55 / -5");
        assertEquals(f.toString(),"-11/1");
        Fraction f2 = new Fraction(11,1);
        assertEquals(f.equals(f2),true);
    }
}